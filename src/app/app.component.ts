import {Component, OnInit} from '@angular/core';
import {Observable, of} from 'rxjs';
import {first} from 'rxjs/operators';
import {Tasksmodel} from './models/tasksmodel';
import {AppresultsService} from './services/app.service';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Twelth Wonder';


  constructor(private appresultsService: AppresultsService) {
  }

  tasks$: Observable<Tasksmodel[]>;
  mytasks: number;
  teamtasks: number;

  ngOnInit() {
    this.getTasksJson();
  }

  private getTasksJson() {
    this.tasks$ = of([]);
    this.appresultsService.getJson().pipe(first()).subscribe((data: Tasksmodel[]) => {
      if (data.length > 0) {
        this.mytasks = data.filter((project: Tasksmodel) => !project.isGlobal).length;
        this.teamtasks = data.length;
        this.tasks$  = of(data);
        this.appresultsService.changeMessage(this.tasks$);
      }
    });
  }
}

export class Tasksmodel {
  text: string;
  isGlobal: true;
  isLeader: false;
  creator: string;
  isCompleted: boolean;
  start: any;
  end: any;
};

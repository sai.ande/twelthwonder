import { NgModule } from '@angular/core';
import {Routes, RouterModule, Router, RouterEvent, RouteConfigLoadStart, RouteConfigLoadEnd} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {TasksComponent} from './tasks/tasks.component';


const routes: Routes = [

  {
    path: '', component: HomeComponent, pathMatch: 'full'
  },
  {
    path: 'tasks', component: TasksComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}

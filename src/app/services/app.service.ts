
import {Tasksmodel} from '../models/tasksmodel';
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable, of, throwError} from 'rxjs';
import {map, catchError, tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class AppresultsService {
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  private messageSource = new BehaviorSubject<any>('');
  currentMessage = this.messageSource.asObservable();

  constructor(private http: HttpClient) {
  }

  changeMessage(message: any) {
    this.messageSource.next(message);
  }

  getJson() {
    return this.http.get(`http://localhost:4200/assets/tasks.json`)
      .pipe(
        map((res: any) => res.tasks),
        catchError(this.handleError<any>('tasks'))
      );

  }

  private handleError<T>(operation = 'operation', result?: any[]) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as unknown as T);
    };
  }

}

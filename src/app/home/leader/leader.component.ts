import {Component, Input, OnInit} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Tasksmodel} from '../../models/tasksmodel';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-leader',
  templateUrl: './leader.component.html',
  styleUrls: ['./leader.component.scss']
})
export class LeaderComponent implements OnInit {

  @Input() data$: Observable<Tasksmodel[]>;
  private dataSource: MatTableDataSource<Tasksmodel>;
  displayedColumns: string[] = ['text', 'creator', 'isCompleted', 'isLeader', 'isGlobal', 'start', 'end'];
  constructor() { }

  ngOnInit() {
    this.data$.subscribe((data: Tasksmodel[]) => {
      data = data.filter((project: Tasksmodel) => project.isLeader);
      this.data$ = of(data);
    });
  }

}

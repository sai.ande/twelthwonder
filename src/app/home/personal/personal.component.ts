import {Component, Input, OnInit} from '@angular/core';
import {Tasksmodel} from '../../models/tasksmodel';
import {MatTableDataSource} from '@angular/material';
import { filter } from 'rxjs/operators';
import {Observable, of} from 'rxjs';


@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.scss']
})
export class PersonalComponent implements OnInit {
  @Input() data$: Observable<Tasksmodel[]>;
  private dataSource: MatTableDataSource<Tasksmodel>;
  displayedColumns: string[] = ['text', 'creator', 'isCompleted', 'isLeader', 'isGlobal', 'start', 'end'];
  constructor() { }

  ngOnInit() {
    this.data$.subscribe((data: Tasksmodel[]) => {
      data = data.filter((project: Tasksmodel) => !project.isGlobal);
      this.data$ = of(data);
    });
  }

}

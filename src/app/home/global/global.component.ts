import {Component, Input, OnInit} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Tasksmodel} from '../../models/tasksmodel';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-global',
  templateUrl: './global.component.html',
  styleUrls: ['./global.component.scss']
})
export class GlobalComponent implements OnInit {

  @Input() data$: Observable<Tasksmodel[]>;
  private dataSource: MatTableDataSource<Tasksmodel>;
  displayedColumns: string[] = ['text', 'creator', 'isCompleted', 'isLeader', 'isGlobal', 'start', 'end'];

  constructor() { }

  ngOnInit() {
    this.data$.subscribe((data: Tasksmodel[]) => {
      data = data.filter((project: Tasksmodel) => project.isGlobal === true);
      this.data$ = of(data);
    });
  }

}

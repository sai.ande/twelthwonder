import {Component, OnInit} from '@angular/core';
import {AppresultsService} from '../services/app.service';
import {first} from 'rxjs/operators';
import {Tasksmodel} from '../models/tasksmodel';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private appresultsService: AppresultsService) {
  }

  tasks$: Observable<Tasksmodel[]>;

  ngOnInit() {
    this.appresultsService.currentMessage.subscribe((response: any) => {
      this.tasks$ = response;
    });
  }

  private getTasksJson() {
    this.tasks$ = of([]);
    this.appresultsService.getJson().pipe(first()).subscribe((data: Tasksmodel[]) => {
      if (data.length > 0) {
        this.tasks$  = of(data);

        this.appresultsService.changeMessage(this.tasks$);
      }
    });
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import {HttpClientModule} from '@angular/common/http';
import { GlobalComponent } from './home/global/global.component';
import { PersonalComponent } from './home/personal/personal.component';
import { LeaderComponent } from './home/leader/leader.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatCardModule,
  MatFormFieldModule,
  MatGridListModule,
  MatListModule,
  MatSidenavModule,
  MatTableModule,
  MatInputModule,
  MatSelectModule,
  MatDialogModule, MatDatepickerModule, MatNativeDateModule, MatButtonModule, MatCheckboxModule
} from '@angular/material';
import { TasksComponent } from './tasks/tasks.component';
import { TasksformComponent } from './tasks/tasksform/tasksform.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GlobalComponent,
    PersonalComponent,
    LeaderComponent,
    TasksComponent,
    TasksformComponent
  ],
  entryComponents: [TasksformComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatCardModule,
    MatGridListModule,
    MatListModule,
    MatSidenavModule,
    MatSelectModule,
    MatDialogModule,
    MatNativeDateModule,
    FormsModule,
    MatDatepickerModule,
    MatButtonModule,
    MatSelectModule,
    MatCardModule,
    MatCheckboxModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

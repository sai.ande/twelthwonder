import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Tasksmodel} from '../models/tasksmodel';
import {MatTableDataSource, MatDialog, MAT_DIALOG_DATA, Sort, MatSort} from '@angular/material';
import {AppresultsService} from '../services/app.service';
import {TasksformComponent} from './tasksform/tasksform.component';

export interface DialogData {
  element: any;
}

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  @Input() data$: Observable<Tasksmodel[]>;
  private dataSource: MatTableDataSource<Tasksmodel>;
  displayedColumns: string[] = ['text', 'creator', 'isCompleted', 'isLeader', 'isGlobal', 'start', 'end'];
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private appresultsService: AppresultsService, public dialog: MatDialog) { }

  ngOnInit() {
    this.appresultsService.currentMessage.subscribe((response: any) => {
        this.data$ = response;
        this.data$.subscribe((data: Tasksmodel[]) => {
          this.dataSource = new MatTableDataSource(data);
          this.dataSource.sort = this.sort;
        });
    });
  }

  openTaskForm() {
    const dialogRef = this.dialog.open(TasksformComponent);
    dialogRef.afterClosed().subscribe((result: any) => {
      console.log(`Dialog result: ${result}`);
      this.data$.subscribe((data: Tasksmodel[]) => {
        const pushingdata = [{text: result.event.text, creator: result.event.creator,
          end: this.formatDate(result.event.end),
          isCompleted: result.event.isCompleted === undefined ? false : result.event.isCompleted,
          isGlobal: result.event.isGlobal === undefined ? false : result.event.isGlobal,
          isLeader: result.event.isLeader === undefined ? false : result.event.isLeader,
          start: this.formatDate(result.event.start)
        }];
        data = [...pushingdata, ...data];
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
        this.data$ = of(data);
        this.appresultsService.changeMessage(this.data$);
      });
    });
  }

  changeeverywhereforleader(element, i) {
    this.data$.subscribe((data: Tasksmodel[]) => {
      data[i] = element;
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.data$ = of(data);
      this.appresultsService.changeMessage(this.data$);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  private formatDate(date) {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
    {
      month = '0' + month;

    }
    if (day.length < 2)  {
      day = '0' + day;
    }

    return [year, month, day].join('-');
  }
}



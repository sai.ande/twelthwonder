import {Component, OnInit, ViewChild, AfterViewInit, Output, EventEmitter, Inject} from '@angular/core';
import {Tasksmodel} from '../../models/tasksmodel';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DialogData} from '../tasks.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-tasksform',
  templateUrl: './tasksform.component.html',
  styleUrls: ['./tasksform.component.scss']
})

export class TasksformComponent implements OnInit {

  @Output() output = new EventEmitter<any>();
  AddTasks: Tasksmodel;
  trueorfalse = [
    {value: true, viewValue: 'True'},
    {value: false, viewValue: 'False'},
  ];
  alert: string;
  tasksFormGroup: FormGroup;

  constructor(public dialogRef: MatDialogRef<TasksformComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  ngOnInit() {
    this.AddTasks = new Tasksmodel();
  }

  public onAdd() {

    const selectedStartDate = this.formatDate(this.AddTasks.start);
    const selectedEndDate = this.formatDate(this.AddTasks.end);
    if (this.AddTasks.text && this.AddTasks.creator && selectedStartDate.toLocaleString() && selectedEndDate.toLocaleString()) {
      this.dialogRef.close({event: this.AddTasks});
    } else {
      this.alert = 'Fill all the Fields';
      setTimeout (() => {
        this.alert = ''; }, 3000);

    }
  }

  private formatDate(date) {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
    {
      month = '0' + month;

    }
    if (day.length < 2)  {
      day = '0' + day;
    }

    return [year, month, day].join('-');
  }
}
